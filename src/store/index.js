import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

import proyecto from "./modules/proyecto.module"
import movimiento from "./modules/movimiento.module"
import tipomovimiento from "./modules/tipomovimiento.module"
import funcionario from "./modules/funcionario.module"
import detallecaja from "./modules/detallecaja.module"


Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    proyecto,
    movimiento,
    tipomovimiento,
    funcionario,
    detallecaja
  },
  //Para persistir la sesion
  plugins: [
    createPersistedState({
      storage: window.sessionStorage
    })
  ]
});
