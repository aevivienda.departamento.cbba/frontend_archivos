//eslint-disable-next-line 
/* eslint-disable */

import ApiService from "../services/api.service";
var _ = require("lodash");
// action types
export const GUARDAR_TTIPO_MOVIMIENTO = "guardarTipoMovimiento";
export const ACTUALIZAR_TTIPO_MOVIMIENTO = "actualizarTipoMovimiento";
export const LISTAR_TTIPO_MOVIMIENTOS = "listarTipoMovimiento";
export const MOSTRAR_TTIPO_MOVIMIENTO = "mostarTipoMovimiento";
export const DESACTIVAR_TTIPO_MOVIMIENTO = "desactivarTipoMovimiento";
export const ACTIVAR_TTIPO_MOVIMIENTO = "activarPoryecto";
// mutation types
export const SET_GUARDAR_TTIPO_MOVIMIENTO = "setGuardarTipoMovimiento";
export const SET_ACTUALIZAR_TTIPO_MOVIMIENTO = "setActualizarTipoMovimiento";
export const SET_LISTAR_TTIPO_MOVIMIENTOS = "setListarTipoMovimiento";
export const SET_MOSTRAR_TTIPO_MOVIMIENTO = "setMostarTipoMovimiento";
export const SET_STATUS_TTIPO_MOVIMIENTO = "setActivetipoMovimientoEnterprise";
export const SET_ERROR = "setError";
const state = {
    tipoMovimientos: {
        data: []
    },
    tipoMovimiento: {},
};

const getters = {
    getDatosTipoMovimientos(state) {
        return state.tipoMovimientos;
    },
    getTipoMovimiento(state) {
        return state.tipoMovimiento;
    }
};

const mutations = {
    [SET_ERROR](state, error) {
        state.error = error;
    },
    [SET_GUARDAR_TTIPO_MOVIMIENTO](state, data) {
        console.log("response", data);
        if(state.tipoMovimientos.current_page==1){
            state.tipoMovimientos.data = _.concat(data, state.tipoMovimientos.data);
            state.tipoMovimientos.data = _.dropRight(state.tipoMovimientos.data);
        }
    },
    [SET_ACTUALIZAR_TTIPO_MOVIMIENTO](state, data) {
        console.log("data up", data);
        state.tipoMovimientos.data = _.filter(state.tipoMovimientos.data, _data => {
            let __data = _data;
            if (_data.id == data.id) {
                __data = _.assign(_data, data);
            }
            return __data
        })
    },
    [SET_LISTAR_TTIPO_MOVIMIENTOS](state, data) {
        state.tipoMovimientos = data;
    },
    [SET_MOSTRAR_TTIPO_MOVIMIENTO](state, data) {
        console.log("datashow", data);
        state.tipoMovimiento = data;
    },
    [SET_STATUS_TTIPO_MOVIMIENTO](state, data) {
        state.tipoMovimientos = _.filter(state.tipoMovimientos, _data => {
            if (_data.id == data.tipoMovimiento.id) {
                _data.status_id = data.tipoMovimiento.status_id;
                _data.status = data.tipoMovimiento.status;
            }
            return data
        })
    },
};

const actions = {
    [GUARDAR_TTIPO_MOVIMIENTO](context, tipoMovimiento) {
        console.log("tipoMovimiento", tipoMovimiento);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.post("api/tipo-movimiento/guardar", tipoMovimiento)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_GUARDAR_TTIPO_MOVIMIENTO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [ACTUALIZAR_TTIPO_MOVIMIENTO](context, tipoMovimiento) {
        console.log("tipoMovimiento upodate", tipoMovimiento);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.put("api/tipo-movimiento/actualizar/" + tipoMovimiento.id, tipoMovimiento)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_ACTUALIZAR_TTIPO_MOVIMIENTO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error update", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [ACTIVAR_TTIPO_MOVIMIENTO](context, payload) {
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.put("api/tipo-movimiento/activar", payload)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_STATUS_TTIPO_MOVIMIENTO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error update", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [DESACTIVAR_TTIPO_MOVIMIENTO](context, payload) {
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.put("api/tipo-movimiento/desactivar", payload)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_STATUS_TTIPO_MOVIMIENTO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error update", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [LISTAR_TTIPO_MOVIMIENTOS](context, payload) {
        console.log("payload", payload);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.get("api/tipo-movimientos", {
                params: payload
            })
                .then((response) => {
                    console.log("ressssp lista",response);
                    context.commit(SET_LISTAR_TTIPO_MOVIMIENTOS, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error", error?.response?.data);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [MOSTRAR_TTIPO_MOVIMIENTO](context, payload) {
        console.log("pay", payload);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.get("api/tipo-movimiento/mostrar/" + payload.id, {
                params: payload
            })
                .then((response) => {
                    console.log(response);
                    context.commit(SET_MOSTRAR_TTIPO_MOVIMIENTO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error", error?.response?.data);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
};
export default {
    state,
    actions,
    mutations,
    getters,
};