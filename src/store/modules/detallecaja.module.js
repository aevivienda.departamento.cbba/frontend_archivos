//eslint-disable-next-line 
/* eslint-disable */

import ApiService from "../services/api.service";
var _ = require("lodash");
// action types
export const GUARDAR_DETALLE_CAJA = "guardarDetalleCaja";
export const ACTUALIZAR_DETALLE_CAJA = "actualizarDetalleCaja";
export const LISTAR_DETALLE_CAJAS = "listarDetalleCaja";
export const MOSTRAR_DETALLE_CAJA = "mostarDetalleCaja";
export const DESACTIVAR_DETALLE_CAJA = "desactivarDetalleCaja";
export const ACTIVAR_DETALLE_CAJA = "activarPoryecto";
// mutation types
export const SET_GUARDAR_DETALLE_CAJA = "setGuardarDetalleCaja";
export const SET_ACTUALIZAR_DETALLE_CAJA = "setActualizarDetalleCaja";
export const SET_LISTAR_DETALLE_CAJAS = "setListarDetalleCaja";
export const SET_MOSTRAR_DETALLE_CAJA = "setMostarDetalleCaja";
export const SET_STATUS_DETALLE_CAJA = "setActivedetalleCajaEnterprise";
export const SET_ERROR = "setError";
const state = {
    detalleCajas: {
        data: []
    },
    detalleCaja: {},
};

const getters = {
    getDatosDetalleCajas(state) {
        return state.detalleCajas;
    },
    getDetalleCaja(state) {
        return state.detalleCaja;
    }
};

const mutations = {
    [SET_ERROR](state, error) {
        state.error = error;
    },
    [SET_GUARDAR_DETALLE_CAJA](state, data) {
        console.log("response", data);
        if(state.detalleCajas.current_page==1){
            state.detalleCajas.data = _.concat(data, state.detalleCajas.data);
            state.detalleCajas.data = _.dropRight(state.detalleCajas.data);
        }
    },
    [SET_ACTUALIZAR_DETALLE_CAJA](state, data) {
        console.log("data up", data);
        state.detalleCajas.data = _.filter(state.detalleCajas.data, _data => {
            let __data = _data;
            if (_data.id == data.id) {
                __data = _.assign(_data, data);
            }
            return __data
        })
    },
    [SET_LISTAR_DETALLE_CAJAS](state, data) {
        state.detalleCajas = data;
    },
    [SET_MOSTRAR_DETALLE_CAJA](state, data) {
        console.log("datashow", data);
        state.detalleCaja = data;
    },
    [SET_STATUS_DETALLE_CAJA](state, data) {
        state.detalleCajas = _.filter(state.detalleCajas, _data => {
            if (_data.id == data.detalleCaja.id) {
                _data.status_id = data.detalleCaja.status_id;
                _data.status = data.detalleCaja.status;
            }
            return data
        })
    },
};

const actions = {
    [GUARDAR_DETALLE_CAJA](context, detalleCaja) {
        console.log("detalleCaja", detalleCaja);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.post("api/detalle-caja/guardar", detalleCaja)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_GUARDAR_DETALLE_CAJA, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [ACTUALIZAR_DETALLE_CAJA](context, detalleCaja) {
        console.log("detalleCaja upodate", detalleCaja);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.put("api/detalle-caja/actualizar/" + detalleCaja.id, detalleCaja)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_ACTUALIZAR_DETALLE_CAJA, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error update", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [ACTIVAR_DETALLE_CAJA](context, payload) {
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.put("api/detalle-caja/activar", payload)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_STATUS_DETALLE_CAJA, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error update", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [DESACTIVAR_DETALLE_CAJA](context, payload) {
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.put("api/detalle-caja/desactivar", payload)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_STATUS_DETALLE_CAJA, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error update", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [LISTAR_DETALLE_CAJAS](context, payload) {
        console.log("payload", payload);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.get("api/detalle-cajas", {
                params: payload
            })
                .then((response) => {
                    console.log("ressssp lista",response);
                    context.commit(SET_LISTAR_DETALLE_CAJAS, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error", error?.response?.data);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [MOSTRAR_DETALLE_CAJA](context, payload) {
        console.log("pay", payload);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.get("api/detalle-caja/mostrar/" + payload.id, {
                params: payload
            })
                .then((response) => {
                    console.log(response);
                    context.commit(SET_MOSTRAR_DETALLE_CAJA, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error", error?.response?.data);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
};
export default {
    state,
    actions,
    mutations,
    getters,
};