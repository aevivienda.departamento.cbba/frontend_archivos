//eslint-disable-next-line 
/* eslint-disable */

import ApiService from "../services/api.service";
var _ = require("lodash");
// action types
export const GUARDAR_MOVIMIENTO = "guardarMovimiento";
export const ACTUALIZAR_MOVIMIENTO = "actualizarMovimiento";
export const LISTAR_MOVIMIENTOS = "listarMovimiento";
export const MOSTRAR_MOVIMIENTO = "mostarMovimiento";
export const DESACTIVAR_MOVIMIENTO = "desactivarMovimiento";
export const ACTIVAR_MOVIMIENTO = "activarPoryecto";
// mutation types
export const SET_GUARDAR_MOVIMIENTO = "setGuardarMovimiento";
export const SET_ACTUALIZAR_MOVIMIENTO = "setActualizarMovimiento";
export const SET_LISTAR_MOVIMIENTOS = "setListarMovimiento";
export const SET_MOSTRAR_MOVIMIENTO = "setMostarMovimiento";
export const SET_STATUS_MOVIMIENTO = "setActivemovimientoEnterprise";
export const SET_ERROR = "setError";
const state = {
    movimientos: {
        data: []
    },
    movimiento: {},
};

const getters = {
    getDatosMovimientos(state) {
        return state.movimientos;
    },
    getMovimiento(state) {
        return state.movimiento;
    }
};

const mutations = {
    [SET_ERROR](state, error) {
        state.error = error;
    },
    [SET_GUARDAR_MOVIMIENTO](state, data) {
        console.log("response", data);
        if(state.movimientos.current_page==1){
            state.movimientos.data = _.concat(data, state.movimientos.data);
            state.movimientos.data = _.dropRight(state.movimientos.data);
        }
    },
    [SET_ACTUALIZAR_MOVIMIENTO](state, data) {
        console.log("data up", data);
        state.movimientos.data = _.filter(state.movimientos.data, _data => {
            let __data = _data;
            if (_data.id == data.id) {
                __data = _.assign(_data, data);
            }
            return __data
        })
    },
    [SET_LISTAR_MOVIMIENTOS](state, data) {
        state.movimientos = data;
    },
    [SET_MOSTRAR_MOVIMIENTO](state, data) {
        console.log("datashow", data);
        state.movimiento = data;
    },
    [SET_STATUS_MOVIMIENTO](state, data) {
        state.movimientos = _.filter(state.movimientos, _data => {
            if (_data.id == data.movimiento.id) {
                _data.status_id = data.movimiento.status_id;
                _data.status = data.movimiento.status;
            }
            return data
        })
    },
};

const actions = {
    [GUARDAR_MOVIMIENTO](context, movimiento) {
        console.log("movimiento", movimiento);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.post("api/movimiento/guardar", movimiento)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_GUARDAR_MOVIMIENTO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [ACTUALIZAR_MOVIMIENTO](context, movimiento) {
        console.log("movimiento upodate", movimiento);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.put("api/movimiento/actualizar/" + movimiento.id, movimiento)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_ACTUALIZAR_MOVIMIENTO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error update", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [ACTIVAR_MOVIMIENTO](context, payload) {
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.put("api/movimiento/activar", payload)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_STATUS_MOVIMIENTO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error update", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [DESACTIVAR_MOVIMIENTO](context, payload) {
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.put("api/movimiento/desactivar", payload)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_STATUS_MOVIMIENTO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error update", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [LISTAR_MOVIMIENTOS](context, payload) {
        console.log("payload", payload);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.get("api/movimientos", {
                params: payload
            })
                .then((response) => {
                    console.log("ressssp lista",response);
                    context.commit(SET_LISTAR_MOVIMIENTOS, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error", error?.response?.data);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [MOSTRAR_MOVIMIENTO](context, payload) {
        console.log("pay", payload);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.get("api/movimiento/mostrar/" + payload.id, {
                params: payload
            })
                .then((response) => {
                    console.log(response);
                    context.commit(SET_MOSTRAR_MOVIMIENTO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error", error?.response?.data);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
};
export default {
    state,
    actions,
    mutations,
    getters,
};