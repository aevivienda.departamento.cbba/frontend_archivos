//eslint-disable-next-line 
/* eslint-disable */

import ApiService from "./../services/api.service";
var _ = require("lodash");
// action types
export const GUARDAR_PROYECTO = "guardarProyecto";
export const ACTUALIZAR_PROYECTO = "actualizarProyecto";
export const LISTAR_PROYECTOS = "listarProyecto";
export const MOSTRAR_PROYECTO = "mostarProyecto";
export const DESACTIVAR_PROYECTO = "desactivarProyecto";
export const ACTIVAR_PROYECTO = "activarPoryecto";
// mutation types
export const SET_GUARDAR_PROYECTO = "setGuardarProyecto";
export const SET_ACTUALIZAR_PROYECTO = "setActualizarProyecto";
export const SET_LISTAR_PROYECTOS = "setListarProyecto";
export const SET_MOSTRAR_PROYECTO = "setMostarProyecto";
export const SET_STATUS_PROYECTO = "setActiveproyectoEnterprise";
export const SET_ERROR = "setError";
const state = {
    proyectos: {
        data: []
    },
    proyecto: {},
};

const getters = {
    getDatosProyectos(state) {
        return state.proyectos;
    },
    getProyecto(state) {
        return state.proyecto;
    }
};

const mutations = {
    [SET_ERROR](state, error) {
        state.error = error;
    },
    [SET_GUARDAR_PROYECTO](state, data) {
        console.log("response", data);
        if(state.proyectos.current_page==1){
            state.proyectos.data = _.concat(data, state.proyectos.data);
            state.proyectos.data = _.dropRight(state.proyectos.data);
        }
    },
    [SET_ACTUALIZAR_PROYECTO](state, data) {
        console.log("data up", data);
        state.proyectos.data = _.filter(state.proyectos.data, _data => {
            let __data = _data;
            if (_data.id == data.id) {
                __data = _.assign(_data, data);
            }
            return __data
        })
    },
    [SET_LISTAR_PROYECTOS](state, data) {
        state.proyectos = data;
    },
    [SET_MOSTRAR_PROYECTO](state, data) {
        console.log("datashow", data);
        state.proyecto = data;
    },
    [SET_STATUS_PROYECTO](state, data) {
        state.proyectos = _.filter(state.proyectos, _data => {
            if (_data.id == data.proyecto.id) {
                _data.status_id = data.proyecto.status_id;
                _data.status = data.proyecto.status;
            }
            return data
        })
    },
};

const actions = {
    [GUARDAR_PROYECTO](context, proyecto) {
        console.log("proyecto", proyecto);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.post("api/proyecto/guardar", proyecto)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_GUARDAR_PROYECTO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [ACTUALIZAR_PROYECTO](context, proyecto) {
        console.log("proyecto upodate", proyecto);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.put("api/proyecto/actualizar/" + proyecto.id, proyecto)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_ACTUALIZAR_PROYECTO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error update", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [ACTIVAR_PROYECTO](context, payload) {
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.put("api/proyecto/activar", payload)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_STATUS_PROYECTO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error update", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [DESACTIVAR_PROYECTO](context, payload) {
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.put("api/proyecto/desactivar", payload)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_STATUS_PROYECTO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error update", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [LISTAR_PROYECTOS](context, payload) {
        console.log("payload", payload);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.get("api/proyectos", {
                params: payload
            })
                .then((response) => {
                    console.log("ressssp lista",response);
                    context.commit(SET_LISTAR_PROYECTOS, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error", error?.response?.data);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [MOSTRAR_PROYECTO](context, payload) {
        console.log("pay", payload);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.get("api/proyecto/mostrar/" + payload.id, {
                params: payload
            })
                .then((response) => {
                    console.log(response);
                    context.commit(SET_MOSTRAR_PROYECTO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error", error?.response?.data);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
};
export default {
    state,
    actions,
    mutations,
    getters,
};