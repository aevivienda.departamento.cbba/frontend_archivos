import ApiService from "./../../services/api.service";
var _ = require("lodash");
// action types


export const STORE_PARTNER_ENTERPRISE = "storePartnerEnterprise";
export const UPDATE_PARTNER_ENTERPRISE = "updatePartnerEnterprise";
export const LIST_PARTNER_ENTERPRISE = "listPartnerEnterprise";
export const SHOW_PARTNER_ENTERPRISE = "showPartnerEnterprise";
export const DEACTIVE_PARTNER_ENTERPRISE = "deactivePartnerEnterprise";
export const ACTIVE_PARTNER_ENTERPRISE = "activePartnerEnterprise";





// mutation types
export const SET_STORE_PARTNER_ENTERPRISE = "setStorePartnerEnterprise";
export const SET_UPDATE_PARTNER_ENTERPRISE = "setUpdatePartnerEnterprise";
export const SET_LIST_PARTNER_ENTERPRISE = "setListPartnerEnterprise";
export const SET_SHOW_PARTNER_ENTERPRISE = "setShowPartnerEnterprise";
export const SET_STATUS_PARTNER_ENTERPRISE = "setActivePartnerEnterprise";





export const SET_ERROR = "setError";
const state = {
    partners: [],
    partner: {}
};

const getters = {
    getPartners(state) {
        return state.partners;
    },
    getPartner(state) {
        return state.partner;
    }
};

const mutations = {
    [SET_ERROR](state, error) {
        state.error = error;
    },
    [SET_STORE_PARTNER_ENTERPRISE](state, data) {
        state.partners =_.concat(data.partner,state.partners);
    },
    [SET_UPDATE_PARTNER_ENTERPRISE](state, data) {
        state.partners=_.filter(state.partners,_data=>{
            if(_data.id==data.partner.id){
                _data.user_id=data.partner.user_id;
                _data.name=data.partner.name;
                _data.lastname=data.partner.lastname;
                _data.cedula=data.partner.cedula;
                _data.email=data.partner.email;
                _data.partner=data.partner.partner;
                _data.enterprise_id=data.partner.enterprise_id;
                _data.country_id=data.partner.country_id;
                _data.state_id=data.partner.state_id;
                _data.city_id=data.partner.city_id;
            }
            return data
        })
    },
    [SET_LIST_PARTNER_ENTERPRISE](state, data) {
        state.partners = data.partners;
    },
    [SET_SHOW_PARTNER_ENTERPRISE](state, data) {
        state.partner = data.partner;
    },
    [SET_STATUS_PARTNER_ENTERPRISE](state, data) {
        state.partners=_.filter(state.partners,_data=>{
            if(_data.id==data.partner.id){
                _data.status_id=data.partner.status_id;
                _data.status=data.partner.status;
            }
            return data
        })
    },
};

const actions = {
    [STORE_PARTNER_ENTERPRISE](context, partner) {
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.post("/backoffice/enterprise/partner/create", partner)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_STORE_PARTNER_ENTERPRISE, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [UPDATE_PARTNER_ENTERPRISE](context, partner) {
        console.log("partner upodate", partner);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.put("/backoffice/enterprise/partner/update", partner)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_UPDATE_PARTNER_ENTERPRISE, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data); 
                })
                .catch((error) => {
                    console.log("error update", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [ACTIVE_PARTNER_ENTERPRISE](context, payload) {
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.put("/backoffice/enterprise/partner/active", payload)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_STATUS_PARTNER_ENTERPRISE, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error update", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [DEACTIVE_PARTNER_ENTERPRISE](context, payload) {
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.put("/backoffice/enterprise/partner/deactive", payload)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_STATUS_PARTNER_ENTERPRISE, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error update", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [LIST_PARTNER_ENTERPRISE](context, payload) {
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.get("/backoffice/enterprise/partner/list", {
                    params: payload
                })
                .then((response) => {
                    context.commit(SET_LIST_PARTNER_ENTERPRISE, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error", error?.response?.data);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [SHOW_PARTNER_ENTERPRISE](context, payload) {
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.get("/backoffice/enterprise/partner/show", {
                params: payload
            })
                .then((response) => {
                    console.log(response);
                    context.commit(SET_SHOW_PARTNER_ENTERPRISE, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error", error?.response?.data);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
};
export default {
    state,
    actions,
    mutations,
    getters,
};