//eslint-disable-next-line 
/* eslint-disable */

import ApiService from "../services/api.service";
var _ = require("lodash");
// action types
export const GUARDAR_FUNCIONARIO = "guardarFuncionario";
export const ACTUALIZAR_FUNCIONARIO = "actualizarFuncionario";
export const LISTAR_FUNCIONARIOS = "listarFuncionario";
export const MOSTRAR_FUNCIONARIO = "mostarFuncionario";
export const DESACTIVAR_FUNCIONARIO = "desactivarFuncionario";
export const ACTIVAR_FUNCIONARIO = "activarPoryecto";
// mutation types
export const SET_GUARDAR_FUNCIONARIO = "setGuardarFuncionario";
export const SET_ACTUALIZAR_FUNCIONARIO = "setActualizarFuncionario";
export const SET_LISTAR_FUNCIONARIOS = "setListarFuncionario";
export const SET_MOSTRAR_FUNCIONARIO = "setMostarFuncionario";
export const SET_STATUS_FUNCIONARIO = "setActivefuncionarioEnterprise";
export const SET_ERROR = "setError";
const state = {
    funcionarios: {
    },
    funcionario: {},
};

const getters = {
    getDatosFuncionarios(state) {
        return state.funcionarios;
    },
    getFuncionario(state) {
        return state.funcionario;
    }
};

const mutations = {
    [SET_ERROR](state, error) {
        state.error = error;
    },
    [SET_GUARDAR_FUNCIONARIO](state, data) {
        console.log("response", data);
        if(state.funcionarios.current_page==1){
            state.funcionarios.data = _.concat(data, state.funcionarios.data);
            state.funcionarios.data = _.dropRight(state.funcionarios.data);
        }
    },
    [SET_ACTUALIZAR_FUNCIONARIO](state, data) {
        console.log("data up", data);
        state.funcionarios.data = _.filter(state.funcionarios.data, _data => {
            let __data = _data;
            if (_data.id == data.id) {
                __data = _.assign(_data, data);
            }
            return __data
        })
    },
    [SET_LISTAR_FUNCIONARIOS](state, data) {
        console.log("datashow", data);
        state.funcionarios = data;
    },
    [SET_MOSTRAR_FUNCIONARIO](state, data) {
        state.funcionario = data;
    },
    [SET_STATUS_FUNCIONARIO](state, data) {
        state.funcionarios = _.filter(state.funcionarios, _data => {
            if (_data.id == data.funcionario.id) {
                _data.status_id = data.funcionario.status_id;
                _data.status = data.funcionario.status;
            }
            return data
        })
    },
};

const actions = {
    [GUARDAR_FUNCIONARIO](context, funcionario) {
        console.log("funcionario", funcionario);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.post("api/funcionario/guardar", funcionario)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_GUARDAR_FUNCIONARIO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [ACTUALIZAR_FUNCIONARIO](context, funcionario) {
        console.log("funcionario upodate", funcionario);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.put("api/funcionario/actualizar/" + funcionario.id, funcionario)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_ACTUALIZAR_FUNCIONARIO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error update", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [ACTIVAR_FUNCIONARIO](context, payload) {
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.put("api/funcionario/activar", payload)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_STATUS_FUNCIONARIO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error update", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [DESACTIVAR_FUNCIONARIO](context, payload) {
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.put("api/funcionario/desactivar", payload)
                .then((response) => {
                    console.log(response);
                    context.commit(SET_STATUS_FUNCIONARIO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error update", error);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [LISTAR_FUNCIONARIOS](context, payload) {
        console.log("payload", payload);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.get("api/funcionarios", {
                params: payload
            })
                .then((response) => {
                    console.log("ressssp lista",response);
                    context.commit(SET_LISTAR_FUNCIONARIOS, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error", error?.response?.data);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
    [MOSTRAR_FUNCIONARIO](context, payload) {
        console.log("pay", payload);
        ApiService.setHeader();
        return new Promise((resolve, reject) => {
            ApiService.get("api/funcionario/mostrar/" + payload.id, {
                params: payload
            })
                .then((response) => {
                    console.log(response);
                    context.commit(SET_MOSTRAR_FUNCIONARIO, response.data);
                    context.commit(SET_ERROR, null);
                    resolve(response.data);
                })
                .catch((error) => {
                    console.log("error", error?.response?.data);
                    context.commit(SET_ERROR, error?.response?.data);
                    reject(error?.response?.data);
                });
        });
    },
};
export default {
    state,
    actions,
    mutations,
    getters,
};