import Vue from "vue";
import VueRouter from "vue-router";
import Dashboard from "../views/pages/Dashboard.vue";
import Caja from "../views/pages/Caja.vue";
import Movimiento from "../views/pages/Movimiento.vue";
import Proyecto from "../views/pages/Proyecto.vue";



Vue.use(VueRouter);

const routes = [
  { path: '/', component: Dashboard, name: "Inicio", meta: { sidemenu: true } },
  { path: '/cajas', component: Caja, name: "Cajas", meta: { sidemenu: true } },
  { path: '/movimientos', component: Movimiento, name: "Movimientos", meta: { sidemenu: true } },
  { path: '/proyectos', component: Proyecto, name: "Proyectos", meta: { sidemenu: true } },


  // { path: '/cajas', component: Box, name: "Box", meta: { sidemenu: true } },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
